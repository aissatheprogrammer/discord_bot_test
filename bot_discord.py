from dotenv import load_dotenv
import os
import discord

load_dotenv() 
discord_key = os.getenv("BOT_KEY")

client = discord.Client()

@client.event
async def on_ready():
    print("Le bot est prêt !")

@client.event
async def on_message(message):
    if "hello" in message.content:
        await message.channel.send("Salut je suis le bot discord")

client.run(discord_key)




 
